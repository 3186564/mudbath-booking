const mysql = require('mysql');
const util = require('util');

const pool = mysql.createPool({
    host: process.env.dbhost,
    user: process.env.dbuser,
    password: process.env.dbpass,
    port: process.env.dbport,
    database: process.env.dbname
})

module.exports = pool;