-- created with mysql  Ver 15.1 Distrib 10.3.8-MariaDB
-- sql schema for api
-- DROP TABLE IF EXISTS bookingdb
-- CREATE DATABASE bookingdb
-- use bookingdb

CREATE TABLE IF NOT EXISTS Room (
  id TINYINT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  name varchar(30)
);

CREATE TABLE IF NOT EXISTS Booking (
  id char(36) NOT NULL PRIMARY KEY,
  room TINYINT NOT NULL,
  firstname varchar(200) NOT NULL,
  surname varchar(200) NOT NULL,
  starttime varchar(60) NOT NULL,
  endtime varchar(60) NOT NULL,
  CONSTRAINT fk_room_id
    FOREIGN KEY (room) REFERENCES Room (id)
    ON DELETE CASCADE
    ON UPDATE RESTRICT
);

INSERT INTO Room (name) VALUES
('boardroom'), ('fishbowl');