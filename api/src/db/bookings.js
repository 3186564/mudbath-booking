const db = require('./pool');

module.exports = {
    getBookings : (callback) => {
        query = `select b.id, b.room, b.firstname, b.surname, b.starttime, b.endtime, r.name as room
                 from Booking b
                 join room r on b.room = r.id`
        db.query(query,(err, results) => {
            if(err) throw err;
            callback(results);
        })
    },
    addBooking : ({id, firstname, surname, starttime, endtime, room}, callback) => {
        query = `insert into Booking(id, firstname, surname, starttime, endtime, room)
                 values(?,?,?,?,?, (select id FROM room where name = ?))`;
        db.query(query, [id, firstname, surname, starttime, endtime, room],(err, result) => {
            console.log(result);
            if(err) throw err;
            callback(true);
        })
    }
}
