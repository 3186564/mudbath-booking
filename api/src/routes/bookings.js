const express = require('express');
const moment = require('moment');
const uuid = require('uuid');
const router = express.Router();
const bookings = require('../db/bookings');

router.get('/', (req, res) => {
    try
    {
        bookings.getBookings((results) => {
            if(results){
                let nowOrLater = results.filter((x) => (moment().isBefore(moment(x.endtime))));
                res.send(nowOrLater);
            }
            else{
                res.send(null);
            }
        });
    }
    catch(err)
    {
        res.sendStatus(500);
    }
});

router.post('/add', (req, res) => {
    try
    {
        let error = false;
        let booking = {...req.body, id: uuid()}
        if(!booking.firstname || !booking.surname || !booking.room || 
           !booking.starttime || !booking.endtime){
            error = true;
        }
        if(!moment(booking.starttime).isValid() || !moment(booking.endtime).isValid()){
            error = true;
        }
        if(moment(booking.starttime).isAfter(booking.endtime)){
            error = true;
        }
        if(!error){
            bookings.addBooking(booking, (success) => {
                if(success){
                    res.send(booking);
                }
                else{
                    res.sendStatus(500);
                }
            });
        }
        else {
            res.sendStatus(400);
        }
  }
  catch(err)
  {
    res.sendStatus(500);
  }
})

module.exports = router;