require('dotenv').config();
const express = require('express');
const cors = require('cors');
const bookings = require('./routes/bookings');
const port = process.env.port || 3000
const app = express();

app.use(express.json());
app.use(cors())
app.use('/bookings', bookings);


app.listen(port, () => {
    console.log(`working on port ${port}`);
});