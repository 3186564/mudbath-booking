const path = require('path');
const webpack = require('webpack');
const HWP = require('html-webpack-plugin');
require('dotenv').config();
module.exports = {
    entry: path.join(__dirname, 'src/app.js'),
    output: {
        filename: 'bundle.js',
        path: path.join(__dirname, '/dist')
    },
    module : {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                loader: "babel-loader"
            }, 
            {   
                test: /\.s?css$/,
                include: [
                    path.resolve(__dirname, "./src"),
                    path.resolve(__dirname, "./node_modules")
                ],
                use: [
                    'style-loader',
                    'css-loader',
                    'sass-loader'
                ]
            }
        ]
    },
    devServer : {
        historyApiFallback: true
    },
    plugins: [
        new HWP(
            {template: path.join(__dirname, 'src/index.html')}
        ),
        new webpack.DefinePlugin({
            "process.env.bookingsurl": JSON.stringify(process.env.bookingsurl)
        })
    ]
}