import moment from 'moment';
import selectBookings from './bookings';

export default (bookings, filters) => {
    let date = filters.date;
    let roomBookings = selectBookings(bookings, filters);
    let times = [];
    let duration = moment.duration(15, 'minutes');
    let start = moment(date).startOf('day').hour(9).minute(0);
    let end =  moment(date).startOf('day').hour(17).minute(0);
    for(start; start <= end; start.add(duration)){
        let option = {
            value:start.format('h:mm a'),
            label: start.format('h:mm a')
        }
        if(roomBookings.some(x => start.isBetween(x.starttime, x.endtime) || start.isSame(x.starttime))){
            option.disabled = true;
        }
        times.push(option)
    }
    return times;
}