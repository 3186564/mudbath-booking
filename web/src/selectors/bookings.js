import moment from 'moment'

export default (bookings, filters) => {
    return bookings.filter((booking) => {
        const room = filters.room === 'all' || booking.room === filters.room;
        const date = moment(booking.starttime).isSame(filters.date, 'day');
        return room && date
    }).sort((x,y) => {
        if(moment(x.starttime).isBefore(moment(y.starttime))){
            return -1;
        }
        else{
            return 1;
        }
    });
}