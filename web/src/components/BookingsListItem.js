import React from 'react';
import moment from 'moment';

const BookingsListItem = ({firstname, surname, starttime, endtime, room}) => (
    <div className="bookinglist-item">
        <h3>{room}</h3>
        <div className="item-details">
            <div className="item__detail">{`${firstname} ${surname}`}</div>
            <div className="item__detail">{`${moment(starttime).format('DD/MM/YY')}`}</div>
            <div className="item__detail item-time">{`${moment(starttime).format('h:mm a')} - ${moment(endtime).format('h:mm a')}`}</div>
        </div>
    </div>
)

export default BookingsListItem;