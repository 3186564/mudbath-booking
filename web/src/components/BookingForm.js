import React from 'react';
import moment from 'moment';
import {SingleDatePicker} from 'react-dates'
import {connect} from 'react-redux'
import 'react-dates/lib/css/_datepicker.css';
import 'react-dates/initialize';
import Select from 'react-select'
import {InitiateAddBooking} from '../actions/bookings';
import selectTimes from '../selectors/availableTimes';

export class BookingForm extends React.Component {
    constructor(props){
        super(props);
        let date = moment();
        let availableTimes = selectTimes(props.bookings, {room:'boardroom', date})
        console.log(props.availableTimes);
        this.state = {
            firstname: '',
            surname: '',
            date: date,
            startTime: undefined,
            endTime: undefined,
            room: 'boardroom',
            error: '',
            focused: false,
            availableTimes: availableTimes,
            availableEndTimes: availableTimes.map(a => ({...a}))
        }
    }
    onFnameChange = (e) => {
        const firstname = e.target.value;
        this.setState(() => ({firstname}));
    }
    onLnameChange = (e) => {
        const surname = e.target.value;
        this.setState(() => ({surname}));
    }
    onDateChange = (date) => {
        const availableTimes = selectTimes(this.props.bookings, {room: this.state.room, date});
        const availableEndTimes = availableTimes;
        this.setState(() => ({date, availableTimes, availableEndTimes, startTime: null, endTime: null}));
    }
    onRoomChange = (selection) => {
        const room = selection.value;
        const availableTimes = selectTimes(this.props.bookings, {room, date:this.state.date});
        const availableEndTimes = availableTimes;
        this.setState(() => ({room, availableTimes, availableEndTimes, startTime: null, endTime: null}));
    }
    isDayBlocked = (date) => {
        const day = date.day();
        return day == 6 || day == 0;
    }
    isEndDisabled = (option) => {
        return option.disabled;
    }
    onStartTimeChange = (selection) => {
        const startTime = selection.value;
        const firstVal = this.state.availableTimes.find((x) => moment(x.value, 'hh:mm a').isAfter(moment(startTime, 'hh:mm a')) && x.disabled);
        console.log(firstVal);
        const firstBooked = firstVal? firstVal.value : undefined;
        const availableEndTimes = this.state.availableEndTimes.map((x) => {
                                    const after = moment(x.value, 'hh:mm a').isAfter(moment(firstBooked, 'hh:mm a'));
                                    const before = moment(x.value, 'hh:mm a').isSameOrBefore(moment(startTime, 'hh:mm a'));
                                    return{
                                        ...x,
                                        disabled: before || after
                                    }
                                })
        this.setState(() => ({startTime: selection, availableEndTimes}));
    }
    onEndTimeChange = (selection) => {
        const endTime = selection;
        this.setState(() => ({endTime}));
    }
    onSubmit = (e) => {
        e.preventDefault();
        if(this.state.firstname === '' || this.state.surname === ''){
            const error = 'Please enter a first and last name for booking'
            this.setState({error});
        }
        if(this.state.firstname.length > 200){
            const error = 'First name must be below 200 characters (try using a nickname)'
            this.setState({error});
        }
        if(this.state.surname.length > 200){
            const error = 'Surname must be below 200 characters (try using an initial)'
            this.setState({error});
        }
        else if(!this.state.startTime || !this.state.endTime){
            const error = 'Please provide a start time and end time for booking'
            this.setState({error});
        }
        else if(moment(this.state.startTime.value, 'hh:mm a').isAfter(moment(this.state.endTime.value, 'hh:mm a'))){
            const error = 'Please ensure booking end time is after start time'
            this.setState({error});
        }
        else if(moment(`${this.state.date.format('DD/MM/YYYY')} ${this.state.startTime.value}`, 'DD/MM/YYYY hh:mm a').isBefore(moment())){
            console.log(moment(`${this.state.date.format('DD/MM/YYY')} ${this.state.startTime.value}`, 'DD/MM/YYYY hh:mm a'))
            console.log(moment());
            const error = 'Please ensure booking starts after now';
            this.setState({error});
        }
        else{
            let date = this.state.date.format('DD/MM/YYYY');
            let starttime = moment(`${date} ${this.state.startTime.value}`, 'DD/MM/YYYY h:mm a');
            let endtime = moment(`${date} ${this.state.endTime.value}`, 'DD/MM/YYYY h:mm a');
            console.log(starttime, endtime)
            this.props.onSubmit({
                firstname: this.state.firstname,
                surname: this.state.surname,
                starttime: starttime,
                endtime: endtime,
                room: this.state.room,
            })
            const error = ''
            this.setState({error});
            this.props.history.push('/')
        }
    }
    render(){
        return(
            <div className="form-container">
                <form onSubmit= {this.onSubmit}>
                {this.state.error && <p>{this.state.error}</p>}
                    <Select
                        className='react-select'
                        placeholder='Room'
                        name='room'
                        onChange= {this.onRoomChange} 
                        options = {[{value:'boardroom', label:'Boardroom'}, {value:'fishbowl', label:'Fishbowl'}]}
                        />
                    <input
                        className="text-input"
                        type="text" 
                        name="fname" 
                        placeholder="First Name"
                        onChange= {this.onFnameChange}
                        />
                    <input
                        className="text-input"
                        type="text" 
                        name="lname" 
                        placeholder="Surname"
                        onChange= {this.onLnameChange} />

                    <SingleDatePicker
                        date={this.state.date}
                        focused = {this.state.focused}
                        onFocusChange={({ focused }) => this.setState({ focused })}
                        onDateChange={date => this.onDateChange(date)}
                        numberOfMonths = {1}
                        isDayBlocked = {this.isDayBlocked.bind(this)}
                        firstDayOfWeek = {1}
                        displayFormat="DD/MM/YYYY"
                    />
                    <br/>
                    <Select
                        value={this.state.startTime}
                        className="react-select"
                        name="starttime"
                        placeholder="Start Time"
                        options={this.state.availableTimes}
                        onChange={this.onStartTimeChange}
                        isOptionDisabled={(option) => option.disabled}
                    />
                    <Select
                        value={this.state.endTime}
                        className="react-select"
                        name="endtime"
                        placeholder="End Time"
                        options={this.state.availableEndTimes}
                        onChange={this.onEndTimeChange}
                        isOptionDisabled={this.isEndDisabled.bind(this)}
                    />
                    <button className="button">Book</button>
                </form>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        bookings: state.bookings,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        onSubmit: (booking) => dispatch(InitiateAddBooking(booking))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(BookingForm);