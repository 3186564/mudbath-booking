import React from 'react';
import {Link} from 'react-router-dom';
import BookingForm from './BookingForm';
import BookingsList from './BookingsList';
import BookingsListFilter from './BookingsListFilter';

const BookingDashboardPage = () => (
    <div>
        <BookingsListFilter />
        <BookingsList />
    </div>
)

export default BookingDashboardPage;