import React from 'react';
import { connect } from 'react-redux';
import moment from 'moment';
import BookingsListItem from './BookingsListItem';
import selectBookings from '../selectors/bookings';

const BookingsList = (props) => (
    <div className='bookingList-container'>
    {
        props.bookings.length === 0 ?  (
            <h2>No Bookings for {props.date.format('MMM Do')}</h2>
        ) : 
        (
            props.bookings.map((booking, index) => <BookingsListItem key={booking.id} {...booking} />)
        )
    }
    </div>
)

const mapStateToProps = (state) => {
    console.log(state.bookings)
    return {
        bookings: selectBookings(state.bookings, state.filters),
        date: state.filters.date
    }
}

export default connect(mapStateToProps)(BookingsList);
