import React from 'react';
import {Link} from 'react-router-dom';

const Header = (props) => 
(
    <header className="header">
        <div className="container">
            <Link to="/" className="title">
                <h1>Room Booking</h1>
            </Link>
        </div>
    </header>
)
export default Header