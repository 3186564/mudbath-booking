import React from 'react';
import {connect} from 'react-redux'
import {Link} from 'react-router-dom'
import {SingleDatePicker} from 'react-dates';
import {setDate, setRoom} from '../actions/filters';
import moment from 'moment';

class BookingsListFilters extends React.Component {
    state = {
        focused: false,
        date : this.props.date
    }
    onDateChange = (date) => {
        this.props.setDate(date);
        this.setState(() => ({date}))
    }
    onRoomChange = (e) => {
        this.props.setRoom(e.target.value);
    }
    render(){
        return (
            <div className="filter-container">
                <div>
                    <select
                        id="room-select"
                        className="select"
                        onChange={this.onRoomChange}>
                        <option selected={this.props.room === 'all'} value='all'>all</option>
                        <option selected={this.props.room === 'boardroom'} value='boardroom'>Boardroom</option>
                        <option selected={this.props.room === 'fishbowl'} value='fishbowl'>FishBowl</option>
                    </select>
                </div>
                <div>
                    <SingleDatePicker
                        date={this.state.date}
                        focused = {this.state.focused}
                        onFocusChange={({ focused }) => this.setState({ focused })}
                        onDateChange={date => this.onDateChange(date)}
                        numberOfMonths = {1}
                        firstDayOfWeek = {1}
                        />
                </div>
                <div>
                    <Link to="/book" className="link-button">Book a room</Link>
                </div>
            </div>
        )
    }
}

const mapDispatchToProps = (dispatch) => ({
    setDate: (date) => dispatch(setDate(date.startOf('day'))),
    setRoom: (room) => dispatch(setRoom(room))
})

const mapStateToProps = (state) => ({
    date: state.filters.date,
    room: state.filters.room
})

export default connect(mapStateToProps, mapDispatchToProps)(BookingsListFilters);