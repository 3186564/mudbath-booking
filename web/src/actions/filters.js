export const setDate = (date = undefined) => ({
    type: 'SET_DATE',
    date
});

export const setRoom = (room = '') => ({
    type: 'SET_ROOM',
    room
});