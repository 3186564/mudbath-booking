import axios from 'axios';

const url = process.env.bookingsurl; //'http://localhost:3000/bookings';
export const InitiateAddBooking = (booking) => {
    return (dispatch) => {
        return axios.post(`${url}/add`, booking)
        .then(response => {
            dispatch(addBooking(response.data))
        })
        .catch(error => {
            throw(error);
        });
    }
}

export const addBooking = (booking) => ({
    type: 'ADD_BOOKING',
    booking
});

export const setBookings = (bookings) => ({
    type: 'SET_BOOKINGS',
    bookings
});

export const getAllBookings = () => {
    return (dispatch) => {
        return axios.get(url)
        .then(response => {
            dispatch(setBookings(response.data))
        })
        .catch(error => {
            throw(error)
        })
    }
}
