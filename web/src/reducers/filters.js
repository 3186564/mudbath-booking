import moment from 'moment';

const defaultState = {
    date: moment(),
    room: 'all',
};

export default (prevState = defaultState, action) => {
    switch(action.type){
        case('SET_DATE'):
            return {
                room: prevState.room,
                date: action.date
            };
        case('SET_ROOM'):
            return {
                room: action.room,
                date: prevState.date
            };
        default: 
            return prevState;
    }
}