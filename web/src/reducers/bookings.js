const defaultState = []
export default (prevState = defaultState, action) => {
    switch(action.type){
        case 'ADD_BOOKING':
            return [...prevState, action.booking]
        case 'SET_BOOKINGS':
            return action.bookings
        default:
            return prevState;
    }
}