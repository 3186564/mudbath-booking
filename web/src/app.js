import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import {BrowserRouter, Route} from 'react-router-dom';
import configureStore from './store/configureStore';
import 'normalize.css/normalize.css'
import BookingDashboardPage from './components/BookingDashboardPage'
import BookingForm from './components/BookingForm'
import {addBooking, getAllBookings} from './actions/bookings';
import Header from './components/Header';
import moment from 'moment'
import './styles/styles.scss';

let store = configureStore();
store.dispatch(getAllBookings());

const routes = (
    <Provider store = {store}>
        <BrowserRouter>
            <div>
                <Header/>
                <Route path="/" component={BookingDashboardPage} exact={true}/>
                <Route path="/book" component={BookingForm} />
            </div>
        </BrowserRouter>
    </Provider>
)


ReactDOM.render(routes, document.getElementById('root'));