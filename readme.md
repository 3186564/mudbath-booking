# MudBath Take Home Challenge App


## How to run
to run both the front end and backend you will need to have node and npm installed. The node version used in development was v10.7.0, and npm 6.4.1
you can download node here: https://nodejs.org/en/
### API
the api requires a mysql database to run. the version used in development was mysql  Ver 15.1 Distrib 10.3.8-MariaDB and can be found here: https://downloads.mariadb.org/

the schema can be found at api/src/db/schema.sql
you can run the sql script in the command line with: 

mysql -u <username\> -p <database name\> < schema.sql

create a .env file in the api directory and add the following with your database details:

dbhost="localhost"  
dbname=""  
dbpass=""  
dbport=  
dbuser=""

you can also add a port variable (port=3000) it will default to 3000 otherwise.

run "npm install" in the api directory  
run "npm start"

### WEB

create a .env file in the web directory.
Create a variable named bookingsurl and point it at your api url + "/bookings" eg: 

bookingsurl="http://localhost:3000/bookings"

you can also add a port variable, it will default to 8080 otherwise.

run "npm install" in the web directory
run "npm run-script build" 
run "npm start"

open your web browser to http://localhost:8080 (or http://localhost: <your port\> if you provided a port)